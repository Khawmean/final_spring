package com.setec.exam.repository;

import java.util.List;

import com.google.common.collect.Lists;
import com.setec.exam.model.Student;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepo extends CrudRepository<Student, Integer> {

    Page<Student> findByNameContaining(String name,Pageable pageable);
    List<Student> findAll();
    Student findById(int id);

}
