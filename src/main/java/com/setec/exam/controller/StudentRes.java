package com.setec.exam.controller;

import java.util.List;


import com.setec.exam.model.Student;
import com.setec.exam.model.request.PageReq;
import com.setec.exam.model.request.StudentReq;
import com.setec.exam.model.respones.BaseResponse;
import com.setec.exam.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("${apiVersion}")
@Api(tags = "Student", value = "SETEC", description = "Top SETEC's student")
public class StudentRes {

    private StudentService studentService;

    @Autowired
    public StudentRes(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/student")
    public BaseResponse<Page<Student>> findAllWithPage(@RequestParam(defaultValue = "") String name, PageReq pageReq){

        BaseResponse<Page<Student>> response = new BaseResponse<>();
        Page<Student> list = studentService.findNameWithPage(name, pageReq);
        response.setData(list);
        response.setHttpStatus(HttpStatus.OK);

        if(list.isEmpty()){
            response.setMessage("Empty");
        }else{
            response.setMessage("Fetch data successfully");
        }
        return response;
    }

    @GetMapping("/students")
    public BaseResponse<List<Student>> findAll(){

        BaseResponse<List<Student>> response = new BaseResponse<>();
        List<Student> list = studentService.findAll();
        response.setData(list);
        response.setHttpStatus(HttpStatus.OK);

        if(list.isEmpty()){
            response.setMessage("Empty");
        }else{
            response.setMessage("Fetch data successfully");
        }
        return response;
    }

    @GetMapping("/student/{id}")
    public BaseResponse<Student> findById(@PathVariable int id){

        BaseResponse<Student> response = new BaseResponse<>();
        Student student = studentService.findById(id);
        if(student == null){
            response.setMessage("Not found");
            response.setData(new Student());
        }else {
            response.setMessage("Fetch data successfully");
            response.setData(student);
        }
        response.setHttpStatus(HttpStatus.OK);
        return response;
    }

    @PostMapping("/student")
    public BaseResponse<Student> insert(@RequestBody StudentReq studentReq){

        BaseResponse<Student> response = new BaseResponse<>();
        Student student = studentService.save(studentReq);
        response.setMessage("Insert student successfully");
        response.setData(student);
        response.setHttpStatus(HttpStatus.OK);

        return response;
    }

    @PutMapping("/student")
    public BaseResponse<Student> update(@RequestBody Student student){

        BaseResponse<Student> response = new BaseResponse<>();
        Student student1 = studentService.update(student);
        if(student1!=null){
            response.setData(student);
            response.setMessage("Update student successfully");
        }else {
            response.setMessage("Not found");
        }
        response.setHttpStatus(HttpStatus.OK);
        return response;
    }

    @DeleteMapping("/student/{id}")
    public BaseResponse<Student> delete(@PathVariable int id){

        BaseResponse<Student> response = new BaseResponse<>();
        Student student = studentService.delete(id);
        if (student!=null){
            response.setMessage("Delete successfully");
            response.setData(student);
        }else {
            response.setMessage("Not found");
        }
        response.setHttpStatus(HttpStatus.OK);

        return response;
    }





}
