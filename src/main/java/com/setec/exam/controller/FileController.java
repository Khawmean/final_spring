package com.setec.exam.controller;
import com.setec.exam.service.upload.FileStorageService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;



@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Api(tags = "File", value = "File", description = "Please upload file and copy file path to insert.")

public class FileController {
    @Value(value = "${file.upload.server.path}")
    private String serverPath;

    @Value("${file.base.url}")
    private String imageUrl;

    @Autowired
    FileStorageService storageService;

    @PostMapping("/upload")
    public ResponseEntity<Map<String,Object>> uploadFile(@RequestParam("image") MultipartFile image) {
        Map<String, Object> res = new HashMap<>();

        String filename = serverPath+image.getOriginalFilename() ;
        try {
            String fileName = storageService.save(image);
            res.put("message","Uploaded the file successfully");
            res.put("status",HttpStatus.OK);
            res.put("fileName",fileName);
            res.put("fullPath",(imageUrl+fileName));

            return ResponseEntity.status(HttpStatus.OK).body(res);
        } catch (Exception e) {
            res.put("error", e.getMessage());
            res.put("message","Could not upload the file:");
            res.put("status error", true);
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(res);
        }
    }

    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = storageService.load(filename);
        System.out.println(file);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

//    @GetMapping("/files")
//    public ResponseEntity<List<FileInfo>> getListFiles() {
//        List<FileInfo> fileInfos = storageService.loadAll().map(path -> {
//            String filename = path.getFileName().toString();
//            String url = MvcUriComponentsBuilder
//                    .fromMethodName(FileController.class, "getFile", path.getFileName().toString()).build().toString();
//            System.out.println(url);
//            return new FileInfo(filename, url);
//
//        }).collect(Collectors.toList());
//        System.out.println(fileInfos.listIterator()
//        );
//        return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
//    }



}
