package com.setec.exam.service;

import java.util.List;

import com.setec.exam.model.Student;
import com.setec.exam.model.request.PageReq;

import com.setec.exam.model.request.StudentReq;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface StudentService {
    Page<Student> findNameWithPage(String name, PageReq PageReq);
    List<Student> findAll();
    Student findById(int id);
    Student save(StudentReq studentReq);
    Student update(Student student);
    Student delete(int id);
}
