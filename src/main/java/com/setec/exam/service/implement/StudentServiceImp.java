package com.setec.exam.service.implement;

import java.util.List;

import com.setec.exam.model.Student;
import com.setec.exam.model.request.PageReq;
import com.setec.exam.model.request.StudentReq;
import com.setec.exam.repository.StudentRepo;
import com.setec.exam.service.StudentService;

import com.setec.exam.utiles.ModelMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;

@Service
public class StudentServiceImp implements StudentService {
    private StudentRepo studentRepo;
    private ModelMap modelMap;

    @Autowired
    public StudentServiceImp(StudentRepo studentRepo, ModelMap modelMap) {
        this.studentRepo = studentRepo;
        this.modelMap = modelMap;
    }

    @Override
    public Page<Student> findNameWithPage(String name, PageReq pageReq) {
        Pageable pageable = PageRequest.of(pageReq.getPage(), pageReq.getOffset(), pageReq.getSortDirection(), pageReq.getSortBy());
        Page<Student> list = studentRepo.findByNameContaining(name,pageable);
        return list;
    }

    @Override
    public List<Student> findAll() {
        return studentRepo.findAll();
    }

    @Override
    public Student findById(int id) {
        return studentRepo.findById(id);
    }

    @Override
    public Student save(StudentReq studentReq) {
        Student student = modelMap.mapper().map(studentReq, Student.class);
        studentRepo.save(student);
       return student;
    }

    @Override
    public Student update(Student student) {
        Student student1 = studentRepo.findById(student.getId());
        if(student1 == null){
            return null;
        }else {
            studentRepo.save(student);
            return student;
        }
    }

    @Override
    public Student delete(int id) {
        Student student = studentRepo.findById(id);
        if(student == null){
            return null;
        }else  {
            studentRepo.deleteById(id);
            return student;
        }
    }
}
