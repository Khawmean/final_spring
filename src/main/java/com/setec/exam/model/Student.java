package com.setec.exam.model;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "tbl_student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String gender;
    private int phone;
    private String email;
    private String profile;


    public Student() {
    }

    public Student( String name, String gender, int phone, String email, String profile) {
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.profile = profile;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getPhone() {
        return this.phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile() {
        return this.profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Student name(String name) {
        setName(name);
        return this;
    }

    public Student gender(String gender) {
        setGender(gender);
        return this;
    }

    public Student phone(int phone) {
        setPhone(phone);
        return this;
    }

    public Student email(String email) {
        setEmail(email);
        return this;
    }

    public Student profile(String profile) {
        setProfile(profile);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            ", gender='" + getGender() + "'" +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", profile='" + getProfile() + "'" +
            "}";
    }


}
