package com.setec.exam.model.request;

public class StudentReq {
    private String name;
    private String gender;
    private int phone;
    private String email;
    private String profile;

    public StudentReq() {
    }

    public StudentReq(String name, String gender, int phone, String email, String profile) {
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
