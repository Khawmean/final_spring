package com.setec.exam.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api(){
        return  new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.setec.exam.controller"))
                .build()
                .apiInfo(apiInfo())
                .enableUrlTemplating(false);
                
    }
    ApiInfo apiInfo() {
        return new ApiInfo(
          "SETEC exam",
          "Group SP10 : This is our final.",
          "1.0.0", 
          "Terms of service", 
          new Contact("setec", "www.setec.com", "setec@info.com"), 
          "License of API", "API license URL", Collections.emptyList());
    }


}
