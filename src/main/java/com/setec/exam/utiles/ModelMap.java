package com.setec.exam.utiles;

import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;

@Component
public class ModelMap {
    public ModelMapper mapper(){
        return new ModelMapper();
    }
}
